(* Counter *)
let counter = ref(0);;
	(** Initialise the steps' counter with 0 **)
	let counter_init() = (counter:=0);;
	
	(** Add a step to the counter **)
	let counter_step() = (counter:= !counter+1);;
	
	(** Get the counter's value : int **)
	let counter_get() = !counter;;

(* Moves' memory *)
let moves = ref [];;
	(** Initialise the moves' counter with nothing **)
	let moves_init() = (moves := []);;
	
	(** Add a movement to the moves list 
			- fromRod : int, numero of the origin rod
			- toRod : int, numero of the destination rod
	**)
	let moves_add fromRod toRod = (moves := (fromRod, toRod) :: !moves;);;
	
	(** Get the moves list from the first one to the last one : (int*int) list **)
	let moves_get() = (List.rev !moves);;

(* Disc Structure *)
module Disc =
	struct
		type disc = {numero : int}
		
		(** Disc creation fonction 
				- n : int, disc's numero
		**)
		let build_disc n = {numero = n}
		
		(** Return disc's numero : int 
			- disc : disc
		**)
		let numero disc = disc.numero
end;;

(* Rod Structure *)
module Rod =
	struct
		type rod = {name : int; mutable discs : Disc.disc list}
		
		(** Rod creation fonction
				- n : int, rod's name
				- discs : disc list, list of the discs present on the rod
		**)
		let build_rod n discs = {name = n; discs = discs}
		
		(** Return the rod name : int **)
		let name rod = rod.name
		
		(** Test if the rod is empty : true if it is, else false 
				- rod : rod
		**)
		let is_empty rod = (List.length rod.discs) = 0
		
		(** Add the disc on the top of the rod's discs
				- rod : rod
				- disc : disc, disc to add to the rod
		**)
		let add rod disc = rod.discs <- (List.cons disc rod.discs)
		
		(** Remove the top disc of the rod
			- rod : rod
		**)
		let remove rod = if is_empty rod then () else (rod.discs <- (List.tl rod.discs))
		
		(** Return the top disc of the rod : disc
				- rod : rod
		**)
		let top rod = List.hd rod.discs
		
		(** Return the discs' list of the rod : disc list
				- rod : rod
		**)
		let get_discs rod = Array.of_list (List.rev rod.discs)
end;;

(* Hanoi Structure *)
module Hanoi =
	struct
		type hanoi = {nb_disc : int; nb_rod : int; mutable rods : Rod.rod array}
		
		(** Return the number of disc used : int
				- hanoi : hanoi
		**)
		let nb_disc hanoi = hanoi.nb_disc
		
		(** Return the number of rod used : int
				- hanoi : hanoi
		**)
		let nb_rod hanoi = hanoi.nb_rod
		
		(** Return the n-th rod
				- hanoi : hanoi
				- n : int, number of the wanted rod
		**)
		let get_rod hanoi n = hanoi.rods.(n)

		(** Build the hanoi tower with n discs and p rods
				- n : int, number of discs -> n >= 0
				- p : int, number of rods -> p > 2
		**)
		let build_hanoi n p = 
			if p < 2 then failwith "solve_hanoi : insuffisant number of rods";
			if n < 0 then failwith "solve_hanoi : insuffisant number of discs";
			let discs = ref [] in
				for i = 0 to (n-1) do
					discs := (Disc.build_disc (n-i)) :: !discs;
				done;
				let rod_array = Array.make (p) (Rod.build_rod 1 !discs) in
					for i=2 to p do
						rod_array.(i-1) <- Rod.build_rod i []
					done;
				{nb_disc = n; nb_rod = p; rods = rod_array}
		
		let move hanoi f t = 
			if (((nb_rod hanoi) > f && f >= 0) && ((nb_rod hanoi) > t && t >= 0) && ((Rod.is_empty (get_rod hanoi f)) = false)) then
				begin	
					let fromRod = (get_rod hanoi f) and toRod = (get_rod hanoi t) in 
						let disc = Rod.top fromRod in
							Rod.remove fromRod;
							Rod.add toRod disc;
							Disc.numero disc;
				end
			else (-1)
end;;
