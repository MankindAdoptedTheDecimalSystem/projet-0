#load "graphics.cma";;	(* use for graphics *)
open Graphics;;

#load "unix.cma";;		(* use to delay operations *)
open Printf;;			(* use to write in files *)

#use "hanoi_modules.ml";;

(* DRAWING FUNCTIONS *)	
(**
   	Draw the animation of the hanoi's towers
   		- hanoi : an object of type hanoi representing the game
   		- disc_moved : int, last disc moved's numero
 **)
let draw hanoi disc_moved = 
  (* clear the graphical windows *)
  clear_graph();

  (* draw the background : the rods *)
  set_line_width 2;
  let rod_space_around = (1000 / (Hanoi.nb_rod hanoi + 1)) in
  for i = 1 to (Hanoi.nb_rod hanoi) do
    fill_poly [|(rod_space_around*i - 10, 0);
                (rod_space_around*i - 10, 300);
                (rod_space_around*i + 10, 300);
                (rod_space_around*i + 10, 0)|];
  done;

  (* draw the hanoi's discs *)
  for i = 0 to (Hanoi.nb_rod hanoi)-1 do
    let rod = Hanoi.get_rod hanoi i in
    let discs = Rod.get_discs rod in
    for j = 0 to (Array.length discs)-1 do
      let rank = ((Disc.numero (discs.(j))) + 1) 
      and nb_disc = Hanoi.nb_disc hanoi in
      let y = (j*20) 
      and x = rod_space_around*(i+1) 
      and size = ((rod_space_around / 2 - rod_space_around / 10) 
                  - (rod_space_around / 2 - rod_space_around / 5)
                    * (nb_disc - rank) / nb_disc) 
      in
      let pointA = (x - size, y)
      and pointB = (x - size, y + 20)
      and pointC = (x + size, y + 20)
      and pointD = (x + size, y) in
      if rank = disc_moved then (set_color red) else (set_color white);
      (* draw the polygon representing the disc *)
      fill_poly [|pointA;pointB;pointC;pointD|];
      set_color black;
      (* draw polygon's border *)
      moveto (fst pointA) (snd pointA); 
      lineto (fst pointB) (snd pointB);
      moveto (fst pointB) (snd pointB); 
      lineto (fst pointC) (snd pointC);
      moveto (fst pointC) (snd pointC); 
      lineto (fst pointD) (snd pointD);
      moveto (fst pointD) (snd pointD); 
      lineto (fst pointA) (snd pointA);
    done;
  done;;

(*MAIN FUNCTIONS*)
(**
   	Apply the movement : add it to the movement list, print it, 
   	and add a step.
   		- origin : int, numero of the origin rod
   		- destination : int, numero of the destination rod
 **)
let movement hanoi origin destination is_animated =
  moves_add origin destination;
  counter_step();
  print_string
    ("I move a disc from rod " ^ (string_of_int origin) ^
     " to rod " ^ (string_of_int destination));
  print_newline();
  if is_animated then
    begin
      let disc = Hanoi.move hanoi origin destination in
      draw hanoi (disc + 1);
      Unix.sleepf 0.5;
    end;;

(**
   	Auxiliary function solving the hanoi's tower problems for n discs 
   	and p rods
   		- hanoi : hanoi, memory of the hanoi's tower
   		- n : int, number of discs used 
   			-> condition : n >= 0
   		- p : int, number of rods used 
   			-> condition : p > 2
   		- from : int, numero of the origin rod 
   			-> condition : 0 <= from < p
   		- toward : int, numero of the destination rod 
   			-> condition : 0 <= toward <p
   		- help_rod : int, numero of a free rod that will store an 
   		eventual excess of discs 
   			-> condition 0 <= help_rod <p
 **)
let rec solve_hanoi_rec hanoi n p from toward help_rod is_animated =
  if p < 2 then failwith "solve_hanoi : insuffisant number of rods";
  if n < 0 then failwith "solve_hanoi : insuffisant number of discs";
  if n = 1 then
    begin
      movement hanoi from toward is_animated;
    end
  else
    begin
      if n > 0 then
        begin
          (*Number of discs that will get their own rod for 
            themsleves.*)
          let helped_discs = min(p-3)(n-1)
          in 
          solve_hanoi_rec 	hanoi 
            (n-1-helped_discs)
            p
            from
            help_rod
            toward 
            is_animated;
          for i = 1 to helped_discs do
            (*Each of remaining discs, except the last, 
              gets its own rod.*)
            solve_hanoi_rec hanoi 
              1 
              p 
              from
              (1+i) 
              help_rod
              is_animated;
          done;
          (* The last disc is moved where it ought to be. *)
          solve_hanoi_rec 	hanoi
            1
            p 
            from
            toward
            help_rod
            is_animated;
          for i = 0 to helped_discs-1 do
            (* At last, every other disc... *)
            solve_hanoi_rec hanoi
              1 
              p 
              (1+(helped_discs-i))
              toward
              help_rod
              is_animated;
          done;
          (* ... is piled over the large one once again. *)
          solve_hanoi_rec 	hanoi 
            (n-1-helped_discs) 
            p
            help_rod
            toward
            from
            is_animated;
        end;
    end;;

(**
   	Solve the hanoi's towers for n discs and p rods
   		- n : int, number of discs used -> condition : n >= 0
   		- p : int, number of rods used -> condition : p > 2
   		- is_animated : boolean, true if you want to see the animation,
   						else false
 **)
let solve_hanoi n p is_animated = 
  if p < 2 then failwith "solve_hanoi : insuffisant number of rods";
  if n < 0 then failwith "solve_hanoi : insuffisant number of discs";
  let hanoi = (Hanoi.build_hanoi n p) in
  counter_init();
  moves_init();
  if (is_animated) then
    begin
      open_graph " 1000x800-0+0";
    end;
  solve_hanoi_rec hanoi (n) (p) (0) (p-1) (1) is_animated;;

(*DATA EXPORTING FUNCTIONS*)
(**
   	Write the data into hanoi.data
   		- name : string, name of the file
   		- data : string, content to write into the file
 **)
let print_file name data =
  let oc = open_out name in
  fprintf oc "%s" data;
  close_out oc;;

(**
   	Write the R script needed to draw the graph
   		- p : int, rods' number of the first hanoi's towers
   			-> condition : p > 2
   		- q : int, rods' number of the second hanoi's towers,
   			  if there isn't one q = (-1)
   			  -> condition q > 2 or q = (-1)
 **)
let print_r_script p q =
  if p < 0 || (q <= 2 && q <> (-1)) then 
    failwith "print_r_script : error impossible value(s)";
  if q = (-1) then
    begin
      let hanoi_r = 
	"data_hanoi <- "
		^"read.table(\"hanoi.data\", sep=\",\", "
		^"col.names=c(\"discs\", \"steps\"))\n"
		^"pdf(\"hanoi_data.pdf\")\n"
		^"plot(data_hanoi, type=\"o\", col=\"blue\")\n"
		^"title(main=\"Hanoi's towers " ^ (string_of_int p) 
		^ " rods\\n-- number of steps by discs --\", "
		^ "col.main=\"red\", font.main=4)\n"
		^ "q()"
      in
      let oc = open_out "hanoi.r" in
      fprintf oc "%s" hanoi_r;
      close_out oc;
    end
  else
    begin
      let hanoi_r =
"data_hanoiP <- "
^"read.table(\"hanoi_P.data\", sep=\",\", "
^"col.names=c(\"discs\", \"steps\"))\n"
^"data_hanoiQ <- read.table(\"hanoi_Q.data\", sep=\",\", "
^"col.names=c(\"discs\", \"steps\"))\n"
^"pdf(\"hanoi_compared_data.pdf\")\n"
^"plot(data_hanoiP, type=\"o\", col=\"blue\", ann=FALSE, "
^"ylim=c(0, max(data_hanoiP, data_hanoiQ)))\n"
^"par(new=TRUE)\n"
^"plot(data_hanoiQ, type=\"o\", col=\"red\", ann=FALSE, "
^"ylim=c(0, max(data_hanoiP, data_hanoiQ)))\n"
^"title(xlab= \"Steps\", col.lab=rgb(0,0.5,0), font.lab=4)\n"
^"title(ylab= \"Discs\", col.lab=rgb(0,0.5,0), font.lab=4)\n"
^"title(main=\"Hanoi's towers\\n-- number of steps by discs --\", "
^"col.main=\"red\", font.main=4)\n"
^"legend(\"topleft\", c(\""^(string_of_int p)^"-rods\", \""
^(string_of_int q)^"-rods\"), cex=0.8, col=c(\"blue\", \"red\"), "
^"lty=1:2, lwd=2, bty=\"n\")\n"
^"q()"
      in
      let oc = open_out "hanoi_extended.r" in
      fprintf oc "%s" hanoi_r;
      close_out oc;
    end;;

(**
   	Generate the data file and R script of the hanoi's towers for p rods
   		- n : int, minimal number of disc 
   			-> condition : n >= 0
   		- m : int, maximal number of disc 
   			-> condition : m >= 0 and m >= n
   		- p : int, number of rods used 
   			-> condition : p > 2
 **)
let generate_data n m p = 
  let data = ref "" in
  for i = n to m do
    Printf.printf "Hanoi towers with %d disc(s) and %d rod(s) : " i p;
    solve_hanoi i p false;
    data := !data ^ (string_of_int i) ^ ","
            ^ (string_of_int (counter_get())) ^ "\n";
    print_newline();
  done;
  print_file "hanoi.data" !data;
  print_r_script p (-1);;

(**
   	Generate the data file and R script of the comparison between 
   	hanoi's towers of p and q rods
   		- n : int, minimal number of disc 
   			-> condition : n >= 0
   		- m : int, maximal number of disc 
   			-> condition : m >= 0 and m >= n
   		- p : int, number of rods used for solving the first 
   			  hanoi's tower
   			-> condition : p > 2
   		- q : int, number of rods used for solving the second
   			  hanoi's tower 
   			-> condition : p > 2
 **)
let generate_compared_data n m p q = 
  let dataP = ref "" and dataQ = ref "" in
  for i = n to m do
    Printf.printf "Hanoi towers with %d disc(s) and %d rod(s) : " i p;
    solve_hanoi i p false;
    print_newline();
    dataP := !dataP ^ (string_of_int i) ^ "," 
             ^ (string_of_int (counter_get())) ^ "\n";

    Printf.printf "Hanoi towers with %d disc(s) and %d rod(s) : " i q;
    solve_hanoi i q false;
    print_newline();
    dataQ := !dataQ ^ (string_of_int i) ^ ","
             ^ (string_of_int (counter_get())) ^ "\n";
  done;
  print_file ("hanoi_P.data") !dataP;
  print_file ("hanoi_Q.data") !dataQ;
  print_r_script p q;;

(* DEMO *)

print_string("Solve the hanoi's towers for 15 discs and 8 rods.");;
solve_hanoi 8 5 true;;

prerr_string "Press enter to continue ...";;
prerr_newline ();;
read_line();;

close_graph();;

print_string ("Compute the data representing the numbers of steps"
^" needed to solve the problem for 0 to 10 discs with 4 and 7 rods.");;
generate_compared_data 0 10 4 7;;
