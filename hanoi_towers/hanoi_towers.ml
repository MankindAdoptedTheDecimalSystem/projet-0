#load "unix.cma";;		(* use to delay operations *)
open Printf;;			(* use to write in files *)

(* Counter *)
let counter = ref(0);;
let counter_init() = (counter:=0);;
let counter_step() = (counter:= !counter+1);;
let counter_get() = !counter;;

(*MAIN FUNCTIONS*)
(**
   	Apply the movement : print it, and add a step
   		- origin : int, numero of the origin rod
   		- destination : int, numero of the destination rod
 **)
let movement origin destination =
  counter_step();
  print_string
    ("I move a disc from rod " ^ origin ^
     " to rod " ^ destination);
  print_newline();;

(**
   	Auxiliary function to solve the hanoi's towers for n discs
   		- n : int, number of discs used
   			-> condition : n >= 0
   		- rods : string array, array representing the 3 rods 
   			-> condition : (Array.length tab) = 3
 **)
let rec solve_hanoi_rec n rods =
  if n < 0 then failwith "solve_hanoi : insuffisant number of disc";
  if n = 1 then
    begin
      movement rods.(0) rods.((Array.length rods) -1);
    end
  else
    begin
      if n <> 0 then
        begin
          solve_hanoi_rec (n-1) [|rods.(0); rods.(2); rods.(1)|];
          solve_hanoi_rec 1 rods;
          solve_hanoi_rec (n-1) [|rods.(1); rods.(0); rods.(2)|];
        end;
    end;;

(**
   	Solve the hanoi's towers for n discs
   		- n : int, number of discs used -> condition : n >= 0
 **)
let solve_hanoi n = counter_init(); solve_hanoi_rec n [|"A";"B";"C"|];;

(*DATA EXPORTING FUNCTIONS*)
(**
   	Write the data into hanoi.data
   		- data : string, string to write into hanoi.data
 **)
let print_file data =
  let oc = open_out "hanoi.data" in
  fprintf oc "%s" data;
  close_out oc;;

(**
   	Generate the data file of the hanoi's towers
   		- n : int, minimal number of disc 
   			-> condition : n >= 0
   		- m : int, maximal number of disc 
   			-> condition : m >= 0 and m >= n
 **)
let generate_data n m = 
  let data = ref "" in
  for i = n to m do
    Printf.printf "Hanoi towers with %d rod(s) : " i;
    solve_hanoi i;
    data := !data ^ (string_of_int i) ^ "," 
            ^ (string_of_int (counter_get())) ^ "\n";
    print_newline();
  done;
  print_file !data;;
  
(* DEMO *)

print_string ("Solve the hanoi's towers problems for 3 rods"
^"and 5 discs.\n");;
solve_hanoi 5;;

prerr_string "Press enter to continue ...";
prerr_newline ();
read_line();;

print_string ("Compute the data representing the numbers of steps"
^" needed to solve the problem for 0 to 10 discs\n");;
generate_data 0 10;;
